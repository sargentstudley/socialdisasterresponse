var express = require ('express');
var app = express();
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var configDB = require('./config/database.js');

// configuration ============

mongoose.connect(configDB.url);

//TODO: Uncomment this require after passport config is created. 
require('./config/passport')(passport); // pass passport for configuration

app.use(morgan('dev')); //log all requests to console. 
app.use(cookieParser()); //read cookies for authentication
app.use(bodyParser()); // get information from html forms. 

app.set('view engine','ejs'); //Tell Express to use ejs for templating. 

app.use(session({ secret: 'usewhatyouhavetosurvive'})); //Session secret
app.use(passport.initialize());
app.use(passport.session()); //Persistent login sessions. 
app.use(flash()); //Use connect flash (not adobe hell-spawn) for flash messages stored in session. 

// Routing ================================
require('./app/routes.js')(app, passport); //Load our routes. Pass in app and passport as arguments. 
require('./app/resources/routes.js')(app); //Load resources REST API. 
require('./app/haves/routes.js')(app); //Load the haves REST API. 
require('./app/needs/routes.js')(app); //Load the needs REST API. 
require('./app/users/routes')(app); //Load the User Home REST API.

// Launch server
app.listen(port);
console.log('Server listening on port ' + port);

