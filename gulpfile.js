var gulp = require('gulp');
var jasmineNode = require('gulp-jasmine-node');

gulp.task('test', function () {
	return gulp.src(['spec/**/*spec.js']).pipe(jasmineNode({
		timeout: 10000,
		color: false
	}));
});

gulp.task('default', ['test']);

