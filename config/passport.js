var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// load the auth variables
var configAuth = require('./auth');

var User = require('../app/models/user');
module.exports = function (passport) {

    //serializes a user.
    passport
        .serializeUser(function (user, done) {
            done(null, user.id);
        });

    //deserializes a user from the database.
    passport.deserializeUser(function (id, done) {
        User
            .findById(id, function (err, user) {
                done(err, user);
            });
    });

    // Local signup ========== Use named strategies since we have more than one.
    // This defines a passport strategy and what to do with a request.
    passport.use('local-signup', new LocalStrategy({
        usernameField: 'email', passwordField: 'password', passReqToCallback: true //Pass the request to the callback.
    }, function (req, email, password, done) {
        // This is the brains of the passport local authentication.  It's async, so we
        // need to use process.nextTick.
        process
            .nextTick(function () {
                //Look for a user with the given email.
                User
                    .findOne({
                        'local.email': email
                    }, function (err, user) {
                        if (err) 
                            return done(err); //We broke.
                        
                        if (user) {
                            return done(null, false, req.flash('signupMessage', 'That email is already taken.')); //We found one, so don't re-create a user.
                        }
                        if (!req.user) {

                            // No user found, we can create a new one.
                            var newUser = new User();
                            newUser.local.email = email;
                            newUser.local.password = newUser.generateHash(password);
                            newUser.homeLocation = {
                                type: 'Point',
                                coordinates: [0.0, 0.0]
                            };
                            newUser.currentLocation = {
                                type: 'Point',
                                coordinates: [0.0, 0.0]
                            };
                            newUser.save(function (err) {
                                if (err) 
                                    throw err;
                                return done(null, newUser);
                            });
                        } else {
                            //We are linking an existing user!
                            var user = req.user;
                            user.local.email = email;
                            user.local.password = user.generateHash(password);
                            user.save(function (err) {
                                if (err) 
                                    throw err;
                                return done(null, user);
                            });
                        }
                    });
            });
    }));

    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, function (req, email, password, done) {
        //This is the callback from the form. Essentially, this function is what handles the authentication request.
        //A user is trying to authenticate, so lets see if we have them in the db.
        User
            .findOne({
                'local.email': email
            }, function (err, user) {
                if (err) {
                    return done(err); //We had an error and we have died and terrible death. Goodbye cruel world.
                }

                if (!user) {
                    //There is no user, so the account being requested doesn't exist. NOPE!
                    return done(null, false, req.flash('loginMessage', 'There is no account by that name. Click sign up to create one.'));
                }

                //Since we have a valid user at this point, make sure the password is correct.
                if (!user.validPassword(password)) {
                    return done(null, false, req.flash('loginMessage', 'The password was incorrect.'));
                }

                // If we get here, both the username and password were correct. Go ahead and
                // return a valid login.
                return done(null, user); //By passing back a user object instead of false, the auth goes through and a cookie/session is established.
            });
    }));

    ///////////// Facebook Login ///////////////
    passport.use(new FacebookStrategy({
        clientID: configAuth.facebookAuth.clientID,
        clientSecret: configAuth.facebookAuth.clientSecret,
        callbackURL: configAuth.facebookAuth.callbackURL,
        profileFields: configAuth.facebookAuth.profileFields,
        passReqToCallback: true
    }, function (req, token, refreshToken, profile, done) {
        // This is the method that gets called when we are authenicating through
        // facebook.
        process
            .nextTick(function () {
                if (!req.user) {
                    //Switch what we do if the user is already logged in.
                    User
                        .findOne({
                            'facebook.id': profile.id
                        }, function (err, user) {
                            if (err) {
                                return done(err); //We ded. We sooooo ded.
                            }

                            if (user) {
                                // If we have previously removed the linked account, but are now adding it back
                                // in again.
                                if (!user.facebook.token) {
                                    user.facebook.token = token;
                                    user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
                                    user.facebook.email = profile.emails[0].value;

                                    user.save(function (err) {
                                        if (err) {
                                            throw err;
                                        }
                                        return done(null, user); //User was found and facebook account was re-linked. Return that user.
                                    });
                                }

                                return done(null, user); //User was found. Return that user.
                            } else {
                                //Create a user with that ID if it was not found.
                                var newUser = new User();
                                //Set all information of user on our user model on a new facebook property.
                                newUser.facebook.id = profile.id;
                                newUser.facebook.token = token;
                                newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
                                newUser.facebook.email = profile.emails[0].value; //Facebook can have many emails. Let's use the first one.

                                newUser.save(function (err) {
                                    if (err) {
                                        throw err;
                                    }

                                    return done(null, newUser);
                                });
                            }
                        });
                } else {
                    // User is already logged in. Pull the user out of the session, then set the
                    // facebook object like you would with a new user.
                    var user = req.user;

                    user.facebook.id = profile.id;
                    user.facebook.token = token;
                    user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
                    user.facebook.email = profile.emails[0].value; //Facebook can have many emails. Let's use the first one.

                    user.save(function (err) {
                        if (err) {
                            throw err; //We ded.
                        }

                        return done(null, user);
                    });
                }
            });
    }));

    ///////////Twitter Authentication///////////
    passport.use(new TwitterStrategy({
        consumerKey: configAuth.twitterAuth.consumerKey,
        consumerSecret: configAuth.twitterAuth.consumerSecret,
        callbackURL: configAuth.twitterAuth.callbackURL,
        passReqToCallback: true
    }, function (req, token, tokenSecret, profile, done) {
        process
            .nextTick(function () {
                if (!req.user) {
                    User
                        .findOne({
                            'twitter.id': profile.id
                        }, function (err, user) {
                            if (err) {
                                done(err);
                            }

                            if (user) {

                                if (!user.twitter.token) {
                                    //Re-link a twitter account that was previously linked, but removed.
                                    user.twitter.id = profile.id;
                                    user.twitter.token = token;
                                    user.twitter.username = profile.username;
                                    user.twitter.displayName = profile.displayName;

                                    user.save(function (err) {
                                        if (err) {
                                            throw err;
                                        }
                                        return done(null, user); //User was found and twitter account was re-linked. Return that user.
                                    });
                                }

                                return done(null, user);
                            } else {
                                var newUser = new User();

                                newUser.twitter.id = profile.id;
                                newUser.twitter.token = token;
                                newUser.twitter.username = profile.username;
                                newUser.twitter.displayName = profile.displayName;

                                newUser.save(function (err) {
                                    if (err) {
                                        throw err;
                                    }
                                    return done(null, newUser);
                                });
                            }
                        });
                } else {
                    var user = req.user;

                    user.twitter.id = profile.id;
                    user.twitter.token = token;
                    user.twitter.username = profile.username;
                    user.twitter.displayName = profile.displayName;

                    user.save(function (err) {
                        if (err) {
                            throw err;
                        }
                        return done(null, user);
                    });
                }
            });
    }));

    //////Google Auth//////
    passport.use(new GoogleStrategy({
        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL,
        passReqToCallback: true
    }, function (req, token, refreshToken, profile, done) {
        process
            .nextTick(function () {
                if (!req.user) {
                    User
                        .findOne({
                            'google.id': profile.id
                        }, function (err, user) {
                            if (err) {
                                return done(err);
                            }

                            if (user) {

                                if (!user.google.token) {
                                    //Re-link a google account that was previously linked, but removed.
                                    user.google.id = profile.id;
                                    user.google.token = token;
                                    user.google.name = profile.displayName;
                                    user.google.email = profile.emails[0].value;

                                    user.save(function (err) {
                                        if (err) {
                                            throw err;
                                        }
                                        return done(null, user); //User was found and google account was re-linked. Return that user.
                                    });
                                }

                                return done(null, user);
                            } else {
                                var newUser = new User();

                                newUser.google.id = profile.id;
                                newUser.google.token = token;
                                newUser.google.name = profile.displayName;
                                newUser.google.email = profile.emails[0].value;

                                newUser.save(function (err) {
                                    if (err) {
                                        throw err;
                                    }
                                    return done(null, newUser);
                                });
                            }
                        });
                } else {
                    var user = req.user;
                    user.google.id = profile.id;
                    user.google.token = token;
                    user.google.name = profile.displayName;
                    user.google.email = profile.emails[0].value;

                    user.save(function (err) {
                        if (err) {
                            throw err;
                        }
                        return done(null, newUser);
                    });
                }
            });
    }));
};