function toMiles(km) {
	return (km * 0.621371);
}

function toKilometers(miles) {
	return (miles * 1.60934);
}

module.exports = {
	toMiles,
	toKilometers
};