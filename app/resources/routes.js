var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var resourceManager = require('./resourceManager')();
var isLoggedIn = require('../common/apiAuthMiddleware').isApiLoggedIn;

module.exports = function (app) {
	//Resource CRUD. 
	app.post('/api/resources', [isLoggedIn, jsonParser], function (req, res) {
		resourceManager.createResource(req.body)
			.then((savedResource) => {
				res.json({ message: 'New resource created with ID of ' + savedResource._id });
			})
			.catch((err) => {
				res.json({ message: 'Error while trying to create new resource: ' + err });
			});
	});

	app.put('/api/resources/:resourceId', [isLoggedIn, jsonParser], function (req, res) {
		var resourceObject = req.body;
		resourceObject._id = req.params.resourceId;
		resourceManager.updateResource(req.body)
			.then(() => {
				res.json({ message: 'Resource Updated OK.' });
			})
			.catch((err) => {
				if (err.notFound) {
					res.status(404).json({message: 'Resource with id ' + req.params.resourceId + ' does not exist.'});
				}
				else {
					res.json({ message: 'Error trying to update resource: ' + err });
				}
				
			});
	});

	app.delete('/api/resources/:resourceId', [isLoggedIn, jsonParser], function (req, res) {
		resourceManager.deleteResourceById(req.params.resourceId)
			.then(() => {
				res.json({ message: 'Resource Deleted OK.' });
			})
			.catch((err) => {
				if (err.notFound) {
					res.status(404).json({ message: 'Resource with ID ' + req.params.resourceId + ' does not exist.' });
				}
				else {
					res.status(500).json({ message: 'Could not delete resource' + err });
				}

			});
	});

	app.get('/api/resources/:resourceId', [isLoggedIn, jsonParser], function (req, res) {
		resourceManager.findResourceById(req.params.resourceId)
			.then((foundResource) => {
				if (foundResource != null) {
					res.json(foundResource);
				}
				else {
					res.status(404).json({ message: 'Resource with ID ' + req.params.resourceId + ' does not exist.' });
				}
			})
			.catch((err) => {
				res.status(500).json({ message: 'Could not search for resource: ' + err });
			});
	});

	app.get('/api/resources', [isLoggedIn, jsonParser], function (req, res) {
		resourceManager.getAllResources()
			.then((allResources) => {
				res.json(allResources);
			})
			.catch((err) => {
				res.status(500).json({ message: 'Could not fetch all resources: ' + err });
			});
	});
};