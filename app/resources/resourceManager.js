
var resourceManager = function(Resource) {
    if (Resource == null) {
        Resource = require('../models/resource'); //If we aren't given a Resource mongoose model to work with, create one.
    }

    this.Resource = Resource;

    resourceManager.createResource = (newResource) => {
        return new Promise((resolve, reject) => {
            var resourceToCreate = new this.Resource({
                name: newResource.name,
                category: newResource.category
            });
            resourceToCreate.save(function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(resourceToCreate);
                }
            });
        });
    
    };
    
    resourceManager.updateResource = (updatedResource) => {
        return new Promise((resolve, reject) => {
            this.Resource.findByIdAndUpdate(updatedResource._id, { $set: updatedResource }, (err, result) => {
                if (!err && result) {
                    resolve();
                }
                else {
                    if (!result && !err) {
                        err = {notFound: true};
                    }
                    reject(err);
                }
            });
        });
    };
    
    resourceManager.deleteResourceById = (id) => {
        return new Promise((resolve, reject) => {
            this.Resource.findByIdAndRemove(id, (err, result) => {
                if (!err && result) {
                    resolve();
                }
                else {
                    if (!result && !err) {
                        err = { notFound: true };
                    }
                    reject(err);
                }
            });
        });
    };
    
    resourceManager.findResourceById = (id) => {
        return new Promise((resolve, reject) => {
            this.Resource.findById(id, (err, result) => {
                if (!err) {
                    resolve(result);
                }
                else {
                    reject(err);
                }
            });
        });
    };
    
    resourceManager.getAllResources = () => {
        return new Promise((resolve, reject) => {
            this.Resource.find({}, (err, result) => {
                if (!err) {
                    resolve(result);
                }
                else {
                    reject(err);
                }
            });
        });
    };

    return resourceManager;
};

module.exports = resourceManager;