var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var UserLocationManager = require('./userLocationManager');
var isLoggedIn = require('../common/apiAuthMiddleware').isApiLoggedIn;

let manager = new UserLocationManager();

module.exports = function (app) {
    app.put('/api/users/:userId/home', [isLoggedIn, jsonParser], (req, res) => {
        let location = req.body;

        if (!Array.isArray(location)) {
            res.status(400).message('Body must contain two number array of longitude and latitude.');
        }

        manager.setUserHomeLocation(req.params.userId, location[0], location[1])
            .then((result) => {
                //TODO: Finish user home storage.
                res.status(200).json();
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({ message: 'User ID: ' + req.params.userId + 'not found.' });
                }
                else {
                    res.status(500).json({ message: 'Could not update user: ' + err });
                }
            });
    });

    app.get('/api/users/:userId/home', [isLoggedIn, jsonParser], (req, res) => {
        manager.getUserHomeLocation(req.params.userId)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({ message: 'User ID: ' + req.params.userId + ' not found.' });
                }
                else {
                    res.status(500).json({ message: 'Error trying to fetch home location for user: ' + err });
                }
            });
    });

    //////
    //User current location routes: 
    //////
    app.put('/api/users/:userId/currentLocation', [isLoggedIn, jsonParser], (req, res) => {
        let location = req.body;

        if (!Array.isArray(location)) {
            res.status(400).message('Body must contain two number array of longitude and latitude.');
        }

        manager.setUserCurrentLocation(req.params.userId, location[0], location[1])
            .then((result) => {
                //TODO: Finish user home storage.
                res.status(200).json();
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({ message: 'User ID: ' + req.params.userId + 'not found.' });
                }
                else {
                    res.status(500).json({ message: 'Could not update user: ' + err });
                }
            });
    });

    app.get('/api/users/:userId/currentLocation', [isLoggedIn, jsonParser], (req, res) => {
        manager.getUserCurrentLocation(req.params.userId)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({ message: 'User ID: ' + req.params.userId + ' not found.' });
                }
                else {
                    res.status(500).json({ message: 'Error trying to fetch home location for user: ' + err });
                }
            });
    });
};