class UserLocationManager {
    constructor(userModel) {
        if (userModel == null) {
            userModel = require('../models/user');
        }
        this.User = userModel;
    }

    setUserHomeLocation(userId, long, lat) {
        return new Promise((resolve, reject) => {
            let geoJsonLocation = {
                type: 'Point',
                coordinates: [long, lat]
            };

            this.User.findByIdAndUpdate(userId,
                { homeLocation: geoJsonLocation },
                (err, result) => {
                    if (!err && result) {
                        resolve(result);
                    }
                    else {
                        if (!result && !err) {
                            err = { notFound: true };
                        }
                        reject(err);
                    }
                });
        });
    }

    getUserHomeLocation(userId) {
        return new Promise((resolve, reject) => {
            this.User.findOne({_id: userId},'homeLocation', (err, result) => {
                if (!err) {
                    resolve(result);
                }
                else {
                    reject(err);
                }
            });
        });
    }

    setUserCurrentLocation(userId, long, lat) {
        return new Promise((resolve, reject) => {
            let geoJsonLocation = {
                type: 'Point',
                coordinates: [long, lat]
            };

            let userLocation = {
                updated: Date.now(),
                coordinates: geoJsonLocation
            };

            this.User.findByIdAndUpdate(userId,
                { currentLocation: userLocation },
                (err, result) => {
                    if (!err && result) {
                        resolve(result);
                    }
                    else {
                        if (!result && !err) {
                            err = { notFound: true };
                        }
                        reject(err);
                    }
                });
        });
    }

    getUserCurrentLocation(userId) {
        return new Promise((resolve, reject) => {
            this.User.findOne({_id: userId},'currentLocation', (err, result) => {
                if (!err) {
                    resolve(result);
                }
                else {
                    reject(err);
                }
            });
        });
    }
}

module.exports = UserLocationManager;