var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var NeedsManager = require('./needManager');
var isLoggedIn = require('../common/apiAuthMiddleware').isApiLoggedIn;

var manager = new NeedsManager();

module.exports = function (app) {
    app.post('/api/needs', [isLoggedIn, jsonParser], (req, res) => {
        manager.createNeed(req.body)
            .then((result) => {
                res.json({ message: 'New need created with id of ' + result._id });
            })
            .catch((err) => {
                res.status(500).json({ message: 'Could not create new need: ' + err });
            });
    });

    app.get('/api/needs/:needId', [isLoggedIn, jsonParser], (req, res) => {
        manager.findNeedById(req.params.needId)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({ message: 'There is no need with id ' + req.params.needId });
                }
                else {
                    res.status(500).json({ message: 'Error while trying to search for need: ' + err });
                }
            });
    });

    app.get('/api/needs', [isLoggedIn, jsonParser], (req, res) => {
        let lat = req.query.lat;
        let long = req.query.long;
        let radius = req.query.radius;
        let units = req.query.units;

        if (units != 'M' && units != 'K') {
            res.status('400').json({message: 'Units must be M for miles or K for kilometers.'});
        }
		
        if (!lat || !long || !radius) {
            res.status('400').json({message: 'query parameters lat, long, and radius must be filled out.'});
        }

        manager.findNeedsWithin(radius,units,lat,long)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                res.status(500).json({message: 'Error while fetching needs within radius: ' + err});
            });
    });

    app.put('/api/needs/:needId', [isLoggedIn, jsonParser], (req, res) => {
        let updatedNeed = req.body;
        updatedNeed._id = req.params.needId;

        manager.updateNeed(updatedNeed)
            .then(() => {
                res.status(200).json();
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({message: 'Need with id ' + updatedNeed._id + ' does not exist'});
                }
                else { 
                    res.status(500).json({message: 'Error while trying to update resource. ' + err});
                }
            });
    });

    app.delete('/api/needs/:needId', [isLoggedIn, jsonParser], (req, res) => {
        manager.deleteNeedById(req.params.needId)
            .then(() => {
                res.status(200).json();
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json('The need with id ' + req.params.needId + ' does not exist.');
                }
                else {
                    res.status(500).json({message: 'Error trying to delete need: ' + err});
                }
            });
    });
};