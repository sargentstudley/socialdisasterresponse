var geoUtils = require('../common/geolocationUtils');

class NeedsManager {
    constructor(needsModel) {
        if (needsModel == null) {
            needsModel = require('../models/need');
        }

        this.Needs = needsModel;
    }

    createNeed(need) {
        return new Promise((resolve, reject) => {
            let newNeed = new this.Needs(need);
            newNeed.save((err) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(newNeed);
                }
            });
        });

    }

    updateNeed(need) {
        return new Promise((resolve, reject) => {
            this.Needs.findByIdAndUpdate(need._id, { $set: need }, (err, result) => {
                if (!err && result) {
                    resolve();
                }
                else {
                    if (!result && !err) {
                        err = { notFound: true };
                    }
                    reject(err);
                }
            });
        });
    }

    deleteNeedById(id) {
        return new Promise((resolve, reject) => {
            this.Needs.findByIdAndRemove(id, (err, result) => {
                if (!err && result) {
                    resolve();
                }
                else {
                    if (!result && !err) {
                        err = { notFound: true };
                    }
                    reject(err);
                }
            });
        });
    }

    findNeedById(id) {
        return new Promise((resolve, reject) => {
            this.Needs.findById(id)
                .populate('resource')
                .exec((err, result) => {
                    if (!err) {
                        resolve(result);
                    }
                    else {
                        reject(err);
                    }
                });
        });
    }

    findNeeds(searchOptions) {
        return new Promise((resolve, reject) => {

            if (!searchOptions) {
                reject({ message: 'searchOptions can not be null.' });
            }
            //Don't allow a 
            if (Object.keys(searchOptions).length === 0 && searchOptions.constructor === Object) {
                reject({ message: 'searchOptions can not be an empty object.' });
            }
            this.Needs.find(searchOptions, (err, result) => {
                if (!err) {
                    resolve(result);
                }
                else {
                    reject(err);
                }
            });
        });
    }

    /**
	 * Finds needs that are within a given distance of a latitude and longitude.
	 * @param {Number} radius 
	 * @param {String} units 
	 * @param {Number} lat 
	 * @param {Number} long 
	 * @returns A promise that resolves with an array of needs found. 
	 */
    findNeedsWithin(radius, units = 'K', lat, long) {
        return new Promise((resolve, reject) => {
            let distance;
            if (units == 'M') {
                distance = geoUtils.toKilometers(radius);
            }
            else {
                distance = radius;
            }


            let coords = [Number(long), Number(lat)];
            this.Needs.find({
                location: {
                    $nearSphere: {
                        $geometry: {
                            type: 'Point',
                            coordinates: coords
                        },
                        $maxDistance: (distance * 1000)
                    }
                }
            },
                (err, result) => {
                    if (!err) {
                        resolve(result);
                    }
                    else {
                        reject(err);
                    }
                });
        });
    }
}
module.exports = NeedsManager;