var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var needSchema = mongoose.Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    resource: { type: Schema.Types.ObjectId, ref: 'Resource' },
    priority: Number,
    location: {
        type: { type: String },
        coordinates: [Number]
    },
    quantity: Number,
    message: String
});

needSchema.index({ location: '2dsphere' });

module.exports = mongoose.model('Need', needSchema);