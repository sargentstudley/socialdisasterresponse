var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
    local: {
        email: String,
        password: String,
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    homeLocation: {
        type: { type: String },
        coordinates: [Number]
    },
    currentLocation: {
        updated: Date,
        coordinates: {
            type: { type: String },
            coordinates: [Number]
        }
    }
});

userSchema.index({
    homeLocation: '2dsphere',
    'currentLocation.coordinates': '2dsphere'
});


//Generate hash method
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//Compare, for authenticating returning users. 
userSchema.methods.compareSync = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);