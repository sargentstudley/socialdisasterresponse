var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var haveSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    resource: { type: Schema.Types.ObjectId, ref: 'Resource' },
    location: {
        type: { type: String },
        coordinates: [Number]
    },
    quantity: Number,
    message: String
});

haveSchema.index({ location: '2dsphere' });

module.exports = mongoose.model('Have', haveSchema);