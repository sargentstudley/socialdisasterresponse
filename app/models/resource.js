var mongoose = require('mongoose');

var resourceSchema = mongoose.Schema({
    name: String,
    category: String
});

module.exports = mongoose.model('Resource', resourceSchema);