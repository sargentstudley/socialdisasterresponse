var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var HavesManager = require('./haveManager');
var isLoggedIn = require('../common/apiAuthMiddleware').isApiLoggedIn;

var manager = new HavesManager();

module.exports = function (app) {
    app.post('/api/haves', [isLoggedIn, jsonParser], (req, res) => {
        manager.createHave(req.body)
            .then((result) => {
                res.json({ message: 'New have created with id of ' + result._id });
            })
            .catch((err) => {
                res.status(500).json({ message: 'Could not create new have: ' + err });
            });
    });

    app.get('/api/haves/:haveId', [isLoggedIn, jsonParser], (req, res) => {
        manager.findHaveById(req.params.haveId)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({ message: 'There is no have with id ' + req.params.haveId });
                }
                else {
                    res.status(500).json({ message: 'Error while trying to search for have: ' + err });
                }
            });
    });

    app.get('/api/haves', [isLoggedIn, jsonParser], (req, res) => {
        let lat = req.query.lat;
        let long = req.query.long;
        let radius = req.query.radius;
        let units = req.query.units;

        if (units != 'M' && units != 'K') {
            res.status('400').json({message: 'Units must be M for miles or K for kilometers.'});
        }
		
        if (!lat || !long || !radius) {
            res.status('400').json({message: 'query parameters lat, long, and radius must be filled out.'});
        }

        manager.findHavesWithin(radius,units,lat,long)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                res.status(500).json({message: 'Error while fetching haves within radius: ' + err});
            });
    });

    app.put('/api/haves/:haveId', [isLoggedIn, jsonParser], (req, res) => {
        let updatedHave = req.body;
        updatedHave._id = req.params.haveId;

        manager.updateHave(updatedHave)
            .then(() => {
                res.status(200).json();
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json({message: 'Have with id ' + updatedHave._id + ' does not exist'});
                }
                else { 
                    res.status(500).json({message: 'Error while trying to update resource. ' + err});
                }
            });
    });

    app.delete('/api/haves/:haveId', [isLoggedIn, jsonParser], (req, res) => {
        manager.deleteHaveById(req.params.haveId)
            .then(() => {
                res.status(200).json();
            })
            .catch((err) => {
                if (err.notFound) {
                    res.status(404).json('The have with id ' + req.params.haveId + ' does not exist.');
                }
                else {
                    res.status(500).json({message: 'Error trying to delete have: ' + err});
                }
            });
    });


};