var geoUtils = require('../common/geolocationUtils');

class HavesManager {
    constructor(havesModel) {
        if (havesModel == null) {
            havesModel = require('../models/have');
        }

        this.Haves = havesModel;
    }

    createHave(have) {
        return new Promise((resolve, reject) => {
            let newHave = new this.Haves(have);
            newHave.save((err) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(newHave);
                }
            });
        });

    }

    updateHave(have) {
        return new Promise((resolve, reject) => {
            this.Haves.findByIdAndUpdate(have._id, { $set: have }, (err, result) => {
                if (!err && result) {
                    resolve();
                }
                else {
                    if (!result && !err) {
                        err = { notFound: true };
                    }
                    reject(err);
                }
            });
        });
    }

    deleteHaveById(id) {
        return new Promise((resolve, reject) => {
            this.Haves.findByIdAndRemove(id, (err, result) => {
                if (!err && result) {
                    resolve();
                }
                else {
                    if (!result && !err) {
                        err = { notFound: true };
                    }
                    reject(err);
                }
            });
        });
    }

    findHaveById(id) {
        return new Promise((resolve, reject) => {
            this.Haves.findById(id)
                .populate('resource')
                .exec((err, result) => {
                    if (!err) {
                        resolve(result);
                    }
                    else {
                        reject(err);
                    }
                });
        });
    }

    findHaves(searchOptions) {
        return new Promise((resolve, reject) => {

            if (!searchOptions) {
                reject({ message: 'searchOptions can not be null.' });
            }
            //Don't allow a 
            if (Object.keys(searchOptions).length === 0 && searchOptions.constructor === Object) {
                reject({ message: 'searchOptions can not be an empty object.' });
            }
            this.Haves.find(searchOptions, (err, result) => {
                if (!err) {
                    resolve(result);
                }
                else {
                    reject(err);
                }
            });
        });
    }

    /**
	 * Finds haves that are within a given distance of a latitude and longitude.
	 * @param {Number} radius 
	 * @param {String} units 
	 * @param {Number} lat 
	 * @param {Number} long 
	 * @returns A promise that resolves with an array of haves found. 
	 */
    findHavesWithin(radius, units = 'K', lat, long) {
        return new Promise((resolve, reject) => {
            let distance;
            if (units == 'M') {
                distance = geoUtils.toKilometers(radius);
            }
            else {
                distance = radius;
            }


            let coords = [Number(long), Number(lat)];
            this.Haves.find({
                location: {
                    $nearSphere: {
                        $geometry: {
                            type: 'Point',
                            coordinates: coords
                        },
                        $maxDistance: (distance * 1000)
                    }
                }
            },
                (err, result) => {
                    if (!err) {
                        resolve(result);
                    }
                    else {
                        reject(err);
                    }
                });
        });
    }
}
module.exports = HavesManager;