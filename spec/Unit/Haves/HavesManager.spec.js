describe('A have manager', () => {
    var HaveMock, managerUnderTest;

    beforeEach(() => {
        HaveMock = require('../../../app/models/have');

        let HavesManager = require('../../../app/haves/haveManager');
        managerUnderTest = new HavesManager(HaveMock);
    });

    it('can create a new have.', (done) => {
        let newHave = {
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I have 5 vials of Inculin I can spare for someone if they need it.',

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(HaveMock.prototype, '$__save')
            .andCallFake((options, callback) => {
                callback();
            });

        managerUnderTest.createHave(newHave)
            .then((result) => {
                expect(HaveMock.prototype.$__save).toHaveBeenCalled();
                expect(result).toBeTruthy();
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });

    });

    it('can find haves near a given point.', (done) => {
        let haveToReturn = {
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I have 5 vials of Inculin I can spare for someone if they need it.',

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };
        let lat = 42.8814263;
        let long = -71.4854486;
        let units = 'M';
        let radius = 1;

        spyOn(HaveMock, 'find')
            .andCallFake((searchParams, callback) => {
                let result = [haveToReturn];
                callback(null, result);
            });

        managerUnderTest.findHavesWithin(radius, units, lat, long)
            .then((result) => {
                expect(Array.isArray(result)).toEqual(true);
                expect(result.length).toBe(1);
                expect(HaveMock.find).toHaveBeenCalled();
                done();
            })
            .catch((err) => {
                fail('Unexpected error/catch when finding haves within distance of given point.' + err);
                done();
            });
    });

    it('can update an existing have.', (done) => {
        let haveToUpdate = {
            _id: '5a1b681140b75b054c3d2f04',
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I have 5 vials of Inculin I can spare for someone if they need it.',

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(HaveMock, 'findByIdAndUpdate')
            .andCallFake((id, options, callback) => {
                let result = haveToUpdate;
                callback(null, result);
            });

        managerUnderTest.updateHave(haveToUpdate)
            .then(() => {
                expect(HaveMock.findByIdAndUpdate).toHaveBeenCalled();
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });
    });

    it('can delete an existing have.', (done) => {
        let haveToDelete = {
            _id: '5a1b681140b75b054c3d2f04',
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I have 5 vials of Inculin I can spare for someone if they need it.',

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(HaveMock, 'findByIdAndRemove')
            .andCallFake((id, callback) => {
                let result = haveToDelete;
                callback(null, result);
            });

        managerUnderTest.deleteHaveById(haveToDelete._id)
            .then(() => {
                expect(HaveMock.findByIdAndRemove).toHaveBeenCalled();
                expect(HaveMock.findByIdAndRemove).toHaveBeenCalledWith(haveToDelete._id, jasmine.any(Function));
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });
    });

    it('can fetch a have by id', (done) => {
        let haveToFind = {
            _id: '5a1b681140b75b054c3d2f04',
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I have 5 vials of Inculin I can spare for someone if they need it.',

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(HaveMock, 'findById')
            .andCallFake((id) => {
                return {
                    populate: function () {
                        return this;
                    },
                    exec: function (callback) {
                        callback(null, haveToFind);
                    }
                };

            });

        managerUnderTest.findHaveById(haveToFind._id)
            .then(() => {
                expect(HaveMock.findById).toHaveBeenCalled();
                expect(HaveMock.findById).toHaveBeenCalledWith(haveToFind._id);
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });
    });

});