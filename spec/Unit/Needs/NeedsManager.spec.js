describe('A need manager', () => {
    var NeedMock, managerUnderTest;

    beforeEach(() => {
        NeedMock = require('../../../app/models/need');

        let NeedsManager = require('../../../app/needs/needManager');
        managerUnderTest = new NeedsManager(NeedMock);
    });

    it('can create a new need.', (done) => {
        let newNeed = {
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I need 5 vials of Inculin I can spare for someone if they need it.',
            priority: 1,

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(NeedMock.prototype, '$__save')
            .andCallFake((options, callback) => {
                callback();
            });

        managerUnderTest.createNeed(newNeed)
            .then((result) => {
                expect(NeedMock.prototype.$__save).toHaveBeenCalled();
                expect(result).toBeTruthy();
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });

    });

    it('can find needs near a given point.', (done) => {
        let needToReturn = {
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I need 5 vials of Inculin I can spare for someone if they need it.',
            priority: 1,

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };
        let lat = 42.8814263;
        let long = -71.4854486;
        let units = 'M';
        let radius = 1;

        spyOn(NeedMock, 'find')
            .andCallFake((searchParams, callback) => {
                let result = [needToReturn];
                callback(null, result);
            });

        managerUnderTest.findNeedsWithin(radius, units, lat, long)
            .then((result) => {
                expect(Array.isArray(result)).toEqual(true);
                expect(result.length).toBe(1);
                expect(NeedMock.find).toHaveBeenCalled();
                done();
            })
            .catch((err) => {
                fail('Unexpected error/catch when finding needs within distance of given point.' + err);
                done();
            });
    });

    it('can update an existing need.', (done) => {
        let needToUpdate = {
            _id: '5a1b681140b75b054c3d2f04',
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I need 5 vials of Inculin I can spare for someone if they need it.',
            priority: 1,

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(NeedMock, 'findByIdAndUpdate')
            .andCallFake((id, options, callback) => {
                let result = needToUpdate;
                callback(null, result);
            });

        managerUnderTest.updateNeed(needToUpdate)
            .then(() => {
                expect(NeedMock.findByIdAndUpdate).toHaveBeenCalled();
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });
    });

    it('can delete an existing need.', (done) => {
        let needToDelete = {
            _id: '5a1b681140b75b054c3d2f04',
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I need 5 vials of Inculin I can spare for someone if they need it.',
            priority: 1,

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(NeedMock, 'findByIdAndRemove')
            .andCallFake((id, callback) => {
                let result = needToDelete;
                callback(null, result);
            });

        managerUnderTest.deleteNeedById(needToDelete._id)
            .then(() => {
                expect(NeedMock.findByIdAndRemove).toHaveBeenCalled();
                expect(NeedMock.findByIdAndRemove).toHaveBeenCalledWith(needToDelete._id, jasmine.any(Function));
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });
    });

    it('can fetch a need by id', (done) => {
        let needToFind = {
            _id: '5a1b681140b75b054c3d2f04',
            user: '5a1b681140b75b054c3d2f03',
            resource: '5a1b682040b75b054c3d2f04',
            quantity: 5,
            message: 'I need 5 vials of Inculin I can spare for someone if they need it.',
            priority: 1,

            'location': {
                type: 'Point',
                coordinates: [
                    -71.4854487,
                    42.8814264
                ]
            }
        };

        spyOn(NeedMock, 'findById')
            .andCallFake((id) => {
                return {
                    populate: function () {
                        return this;
                    },
                    exec: function (callback) {
                        callback(null, needToFind);
                    }
                };

            });

        managerUnderTest.findNeedById(needToFind._id)
            .then(() => {
                expect(NeedMock.findById).toHaveBeenCalled();
                expect(NeedMock.findById).toHaveBeenCalledWith(needToFind._id);
                done();
            })
            .catch((err) => {
                expect(err).toBeFalsy();
                done();
            });
    });

});