describe('A ResourceManager', () => {

    var ResourceMock, managerUnderTest;


    //Create mock methods for each method call. 

    beforeEach(() => {
        ResourceMock = require('../../../app/models/resource');
        managerUnderTest = require('../../../app/resources/resourceManager')(ResourceMock);
    });

    it('can create a new resource', (done) => {
        var newTestResource = {
            name: 'Bottled Water',
            category: 'Water'
        };

        spyOn(ResourceMock.prototype, '$__save')
            .andCallFake(function (options, callback) {
                callback();
            });
        managerUnderTest.createResource(newTestResource)
            .then((result) => {
                expect(ResourceMock.prototype.$__save).toHaveBeenCalled();
                done();
            })
            .catch((err) => {
                expect(err).toBeNull();
                done();
            });
    });

    it('can return an error if there is an unexpected problem when creating.', (done) => {
        var newTestResource = {
            name: 'Bottled Water',
            category: 'Water'
        };

        spyOn(ResourceMock.prototype, '$__save')
            .andCallFake(function (options, callback) {
                callback({ message: 'Test Error' });
            });

        managerUnderTest.createResource(newTestResource)
            .then((result) => {
                done();
            })
            .catch((err) => {
                expect(ResourceMock.prototype.$__save).toHaveBeenCalled();
                expect(err).toBeTruthy();
                done();
            });
    });

    it('can update an existing resource.', (done) => {
        var resourceToUpdate = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findByIdAndUpdate')
            .andCallFake((id, data, callback) => {
                callback(null, data);
            });

        managerUnderTest.updateResource(resourceToUpdate)
            .then(() => {
                expect(ResourceMock.findByIdAndUpdate).toHaveBeenCalled();
                done();
            })
            .catch((err) => {
                expect(err).toBeNull();
                done();
            });
    });

    it('can report that a resource was not found.', (done) => {
        var resourceToUpdate = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findByIdAndUpdate')
            .andCallFake((id, data, callback) => {
                callback(null, null);
            });

        managerUnderTest.updateResource(resourceToUpdate)
            .then(() => {
                fail('Update resource call succeeded when should have failed.');
                done();
            })
            .catch((err) => {
                expect(ResourceMock.findByIdAndUpdate).toHaveBeenCalled();
                expect(err).toBeTruthy();
                expect(err.notFound).toEqual(true);
                done();
            });
    });

    it('can return an unexpected error when updating.', (done) => {
        var resourceToUpdate = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findByIdAndUpdate')
            .andCallFake((id, data, callback) => {
                callback({ message: 'Test Error' }, null);
            });

        managerUnderTest.updateResource(resourceToUpdate)
            .then(() => {
                fail('Update resource call succeeded when should have failed.');
                done();
            })
            .catch((err) => {
                expect(ResourceMock.findByIdAndUpdate).toHaveBeenCalled();
                expect(err).toBeTruthy();
                done();
            });
    });

    it('can delete a resource', (done) => {
        var resourceToDelete = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findByIdAndRemove')
            .andCallFake((id, callback) => {
                callback(null, resourceToDelete);
            });

        managerUnderTest.deleteResourceById(resourceToDelete._id)
            .then(() => {
                expect(ResourceMock.findByIdAndRemove).toHaveBeenCalled();
                expect(ResourceMock.findByIdAndRemove).toHaveBeenCalledWith(resourceToDelete._id, jasmine.any(Function));
                done();
            })
            .catch((err) => {

                fail('Resource manager returned reject when resolve expected: ' + err);
                done();
            });
    });

    it('can report that a resource to delete was not found.', (done) => {
        var resourceToDelete = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findByIdAndRemove')
            .andCallFake((id, callback) => {
                callback(null, null);
            });

        managerUnderTest.deleteResourceById(resourceToDelete._id)
            .then(() => {
                fail('Resource manager resolved promise when rejection was expected.');
                done();
            })
            .catch((err) => {
                expect(ResourceMock.findByIdAndRemove).toHaveBeenCalled();
                expect(ResourceMock.findByIdAndRemove).toHaveBeenCalledWith(resourceToDelete._id, jasmine.any(Function));
                expect(err.notFound).toEqual(true);
                done();
            });
    });

    it('can report an unexpected error when trying to delete a resource.', (done) => {
        var resourceToDelete = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findByIdAndRemove')
            .andCallFake((id, callback) => {
                callback({ message: 'test error message' }, null);
            });

        managerUnderTest.deleteResourceById(resourceToDelete._id)
            .then(() => {
                fail('Resource manager resolved promise when rejection was expected.');
                done();
            })
            .catch((err) => {
                expect(ResourceMock.findByIdAndRemove).toHaveBeenCalled();
                expect(ResourceMock.findByIdAndRemove).toHaveBeenCalledWith(resourceToDelete._id, jasmine.any(Function));
                expect(err.message).toBeTruthy();
                done();
            });
    });

    it('can find a resource by ID', (done) => {
        var resourceToFind = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findById')
            .andCallFake((id, callback) => {
                callback(null, resourceToFind);
            });

        managerUnderTest.findResourceById(resourceToFind._id)
            .then((result) => {
                expect(ResourceMock.findById).toHaveBeenCalled();
                expect(ResourceMock.findById).toHaveBeenCalledWith(resourceToFind._id, jasmine.any(Function));
                expect(result).toEqual(resourceToFind);
                done();
            })
            .catch((err) => {
                fail('Resource Manager rejected promise when resolve was expected.');
                done();
            });
    });

    it('returns null if id is not found', (done) => {
        var resourceToFind = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findById')
            .andCallFake((id, callback) => {
                callback(null, null);
            });

        managerUnderTest.findResourceById(resourceToFind._id)
            .then((result) => {
                expect(ResourceMock.findById).toHaveBeenCalled();
                expect(ResourceMock.findById).toHaveBeenCalledWith(resourceToFind._id, jasmine.any(Function));
                expect(result).toBeNull();
                done();
            })
            .catch((err) => {
                fail('Resource Manager rejected promise when resolve was expected.');
                done();
            });
    });

    it('can report an unexpected error when trying to find a resource by id.', (done) => {
        var resourceToFind = {
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        };

        spyOn(ResourceMock, 'findById')
            .andCallFake((id, callback) => {
                callback({ message: 'Test error message' }, null);
            });

        managerUnderTest.findResourceById(resourceToFind._id)
            .then((result) => {
                fail('Resource manager resolved promise when reject was expected.');
                done();
            })
            .catch((err) => {
                expect(ResourceMock.findById).toHaveBeenCalled();
                expect(ResourceMock.findById).toHaveBeenCalledWith(resourceToFind._id, jasmine.any(Function));
                expect(err.message).toBeTruthy();
                done();
            });
    });

    it('returns a list of all resources.', (done) => {
        var resourcesToFind = [{
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        },
        {
            name: 'Canned Soup',
            category: 'Cold Food',
            _id: 'zyx987wsr654qpo321nml'
        }];

        spyOn(ResourceMock, 'find')
            .andCallFake((searchParams, callback) => {
                callback(null, resourcesToFind);
            });

        managerUnderTest.getAllResources()
            .then((result) => {
                expect(ResourceMock.find).toHaveBeenCalled();
                expect(ResourceMock.find).toHaveBeenCalledWith({}, jasmine.any(Function));
                expect(result).toEqual(resourcesToFind);
                done();
            })
            .catch((err) => {
                fail('Resource Manager rejected promise when resolve was expected during get all resources.');
                done();
            });
    });

    it('can report an unexpected error when trying to fetch all resources.', (done) => {
        var resourcesToFind = [{
            name: 'Bottled Water',
            category: 'Water',
            _id: '123abc456def789ghi'
        },
        {
            name: 'Canned Soup',
            category: 'Cold Food',
            _id: 'zyx987wsr654qpo321nml'
        }];

        spyOn(ResourceMock, 'find')
            .andCallFake((searchParams, callback) => {
                callback({ message: 'Test Error Message' }, null);
            });

        managerUnderTest.getAllResources()
            .then((result) => {

                fail('Expected promise rejection but it was resolved when fetching all resources.');
                done();
            })
            .catch((err) => {
                expect(ResourceMock.find).toHaveBeenCalled();
                expect(ResourceMock.find).toHaveBeenCalledWith({}, jasmine.any(Function));
                expect(err.message).toBeTruthy();
                done();
            });
    });
}); 